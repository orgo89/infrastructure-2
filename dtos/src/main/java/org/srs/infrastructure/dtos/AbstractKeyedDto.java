package org.srs.infrastructure.dtos;

import java.io.Serializable;
import java.util.Objects;

/**
 * @param <T> The identifier value type.
 * @author Diego López
 *         <p>
 *         Parent class for those DTOs whom need to have
 *         a kind of identifier (like primary key).
 */
public abstract class AbstractKeyedDto<T> implements Serializable {

    private static final long serialVersionUID = -5105517419067418794L;

    // region Fields

    protected T pk;

    // endregion Fields

    // region Constructors

    protected AbstractKeyedDto() {
    }

    protected AbstractKeyedDto(T pk) {
        this.pk = pk;
    }

    // endregion Constructors

    // region Public methods

    public T getPk() {
        return pk;
    }

    public void setPk(T pk) {
        this.pk = pk;
    }

    /**
     * Fluent setter for this entity primary key.
     *
     * @param pk Primary key.
     * @return Current entity instance.
     */
    public AbstractKeyedDto<T> withPk(T pk) {

        setPk(pk);
        return this;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;

        if (!(o instanceof AbstractKeyedDto))
            return false;

        AbstractKeyedDto<?> that = (AbstractKeyedDto<?>) o;

        return Objects.equals(pk, that.pk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pk);
    }

    @Override
    public String toString() {
        return "AbstractKeyedDto{" +
                "pk=" + pk +
                '}';
    }

    // endregion Public methods

}
