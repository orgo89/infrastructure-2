package org.srs.infrastructure.dtos;

import org.srs.infrastructure.enums.OperationStatus;
import org.srs.infrastructure.enums.ProcessStatus;

import java.io.Serializable;

/**
 * DTO that describes basic process response.
 */
public class DtoResponse implements Serializable {

    // region Variables

    /**
     * Message to describe some important action or event satatus.
     */
    private String message;

    /**
     * Describes the whole process status.
     */
    private ProcessStatus processStatus;

    /**
     * Describes the specific action status.
     */
    private OperationStatus operationStatus;

    // endregion Variables

    // region Constructors

    /**
     * Initializes this instance with a null message, {@link ProcessStatus#SUCCESSFUL}
     * and {@link OperationStatus#SUCCESSFUL}.
     */
    public DtoResponse() {
        this.processStatus = ProcessStatus.SUCCESSFUL;
        this.operationStatus = OperationStatus.SUCCESSFUL;
    }

    /**
     * Initializes this instance with {@link OperationStatus#SUCCESSFUL} and the message
     * and {@link ProcessStatus} specified.
     *
     * @param message       Message to describe some important action or event status.
     * @param processStatus Describes the whole process status.
     */
    public DtoResponse(final String message, final ProcessStatus processStatus) {
        this.message = message;
        this.processStatus = processStatus;
        this.operationStatus = OperationStatus.SUCCESSFUL;
    }

    /**
     * Initializes this instance with specified values.
     *
     * @param message         Message to describe some important action or event status.
     * @param processStatus   Describes the whole process status.
     * @param operationStatus Describes the specific action status.
     */
    public DtoResponse(final String message, final ProcessStatus processStatus, final OperationStatus operationStatus) {
        this.message = message;
        this.processStatus = processStatus;
        this.operationStatus = operationStatus;
    }

    // endregion Constructors

    // region Public methods

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public ProcessStatus getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(final ProcessStatus processStatus) {
        this.processStatus = processStatus;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(final OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    public DtoResponse withMessage(final String message) {
        setMessage(message);
        return this;
    }

    public DtoResponse withProcessStatus(final ProcessStatus processStatus) {
        setProcessStatus(processStatus);
        return this;
    }

    public DtoResponse withOperationStatus(final OperationStatus operationStatus) {
        setOperationStatus(operationStatus);
        return this;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DtoResponse that = (DtoResponse) o;

        if (getMessage() != null ? !getMessage().equals(that.getMessage()) : that.getMessage() != null) return false;
        if (getProcessStatus() != that.getProcessStatus()) return false;
        return getOperationStatus() == that.getOperationStatus();
    }

    @Override
    public int hashCode() {
        int result = getMessage() != null ? getMessage().hashCode() : 0;
        result = 31 * result + (getProcessStatus() != null ? getProcessStatus().hashCode() : 0);
        result = 31 * result + (getOperationStatus() != null ? getOperationStatus().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DtoResponse[" +
                "message='" + message + '\'' +
                ", processStatus=" + processStatus +
                ", operationStatus=" + operationStatus +
                ']';
    }

    // endregion Public methods

}
