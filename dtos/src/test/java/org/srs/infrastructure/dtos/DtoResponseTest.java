package org.srs.infrastructure.dtos;

import org.meanbean.test.BeanTester;
import org.testng.annotations.Test;

public class DtoResponseTest {

    @Test
    public void testDtoItself() {
        new BeanTester().testBean(DtoResponse.class);
    }
}