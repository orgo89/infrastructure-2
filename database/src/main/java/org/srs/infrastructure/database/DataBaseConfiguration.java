package org.srs.infrastructure.database;

import org.srs.infrastructure.enums.DataBaseType;

/**
 * Created by Diego on 27/10/2016.
 * <p>
 * Simple entity to represent data base configuration.
 */
public class DataBaseConfiguration {

    // region Properties

    /**
     * Current data base name.
     */
    private String databasename;

    /**
     * Current data base server connection port.
     */
    private String port;

    /**
     * Current data base server ip.
     */
    private String ip;

    /**
     * Data base connection user.
     */
    private String user;

    /**
     * Data base connection user password.
     */
    private String password;

    /**
     * Specifies the current data base type.
     */
    private DataBaseType dbtype;

    // endregion Properties

    // region Public methods

    // region Getters and Setters

    public String getDatabasename() {
        return databasename;
    }

    public void setDatabasename(String databasename) {
        this.databasename = databasename;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DataBaseType getDbtype() {
        return dbtype;
    }

    public void setDbtype(DataBaseType dbtype) {
        this.dbtype = dbtype;
    }

    // endregion Getters and Setters

    // endregion Public methods

}
