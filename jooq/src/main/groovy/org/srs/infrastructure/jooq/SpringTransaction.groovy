package org.srs.infrastructure.jooq

import org.jooq.Transaction
import org.springframework.transaction.TransactionStatus

/**
 * Created by diegom on 9/21/14.
 * <p/>
 * Represents a spring transaction.
 */
class SpringTransaction implements Transaction {

    /**
     * Current transaction.
     */
    private final TransactionStatus transaction

    /**
     * Initializes the transaction object.
     *
     * @param transaction A Transaction object.
     */
    public SpringTransaction(TransactionStatus transaction) {

        this.transaction = transaction
    }

    /**
     * Gets the current TransactionStatus object.
     *
     * @return TransactionStatus object.
     */
    public TransactionStatus getTransaction() {
        return transaction
    }
}
