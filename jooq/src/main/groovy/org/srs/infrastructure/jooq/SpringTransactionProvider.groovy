package org.srs.infrastructure.jooq

import org.jooq.TransactionContext
import org.jooq.TransactionProvider
import org.jooq.exception.DataAccessException
import org.jooq.tools.JooqLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.transaction.TransactionStatus
import org.springframework.transaction.support.DefaultTransactionDefinition

import static org.springframework.transaction.TransactionDefinition.PROPAGATION_NESTED

/**
 * Created by diegom on 9/21/14.
 * <p/>
 * Implements a transaction provider and manages the transaction states.
 */
class SpringTransactionProvider implements TransactionProvider {

    /**
     * Current jooq logger.
     */
    private static final JooqLogger log = JooqLogger.getLogger(SpringTransactionProvider.class)

    @Autowired
    private DataSourceTransactionManager dstm

    /**
     * Begin a new transaction.
     * <p/>
     * This method begins a new transaction with a {@link org.jooq.Configuration} scoped
     * for this transaction. The resulting {@link org.jooq.Transaction} object may be
     * used by implementors to identify the transaction when
     * {@link #commit(org.jooq.TransactionContext)} or
     * {@link #rollback(org.jooq.TransactionContext)} is called.
     *
     * @param ctx @throws org.jooq.exception.DataAccessException Any exception issued by the underlying
     *                   database.
     */
    @Override
    public void begin(TransactionContext ctx) throws DataAccessException {

        log.info("Begin transaction")

        // This TransactionProvider behaves like jOOQ's DefaultTransactionProvider,
        // which supports nested transactions using Savepoints
        TransactionStatus tx = dstm.getTransaction(new DefaultTransactionDefinition(PROPAGATION_NESTED))

        ctx.transaction(new SpringTransaction(tx))
    }

    /**
     * @param ctx @throws org.jooq.exception.DataAccessException Any exception issued by the underlying
     *                   database.
     */
    @Override
    public void commit(TransactionContext ctx) throws DataAccessException {

        log.info("commit transaction")

        dstm.commit(((SpringTransaction) ctx.transaction()).getTransaction())

    }

    /**
     * @param ctx @throws org.jooq.exception.DataAccessException Any exception issued by the underlying
     *                   database.
     */
    @Override
    public void rollback(TransactionContext ctx) throws DataAccessException {

        log.info("rollback transaction")

        dstm.rollback(((SpringTransaction) ctx.transaction()).getTransaction())

    }
}
