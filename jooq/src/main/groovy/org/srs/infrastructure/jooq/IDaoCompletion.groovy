package org.srs.infrastructure.jooq

import org.jooq.exception.DataAccessException

/**
 * Created by Diego on 13/01/2017.
 *
 * Specifies some jooq dao missing methods.
 * This should be implemented over custom dao that extends of jooq generated dao.
 *
 * @param P Pojo type.
 */
interface IDaoCompletion<P> {
    // region Methods

    /**
     * Perform an insert action.
     *
     * @param entity Entity to be inserted.
     *
     * @return Complete data base fetched entity.
     *
     * @throws DataAccessException - if something went wrong executing the query.
     */
    P insertAndFetch(P entity) throws DataAccessException

    /**
     * Perform an update action.
     *
     * @param entity Entity to be updated.
     *
     * @return Complete data base fetched updated entity.
     */
    P updateAndFetch(P entity)

    // endregion Methods
}