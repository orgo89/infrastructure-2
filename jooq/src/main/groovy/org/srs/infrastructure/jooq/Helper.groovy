package org.srs.infrastructure.jooq

import org.jooq.Configuration
import org.jooq.Field
import org.jooq.Record
import org.jooq.Table
import org.jooq.UpdatableRecord
import sun.reflect.CallerSensitive

import static org.jooq.impl.DSL.using

/**
 * Created by Diego on 16/01/2017.
 *
 * Helper class for Jooq.
 * Contains methods to help jooq records (write and read) creation
 * and much more.
 */
class Helper {
    // region Public static methods

    /**
     * Generates a collection of records from pojo's.
     *
     * @param configuration Current data base configuration.
     * @param table Table specification to create new records.
     * @param objects Pojo's collection.
     * @param forUpdate Specifies if is an insert or update.
     *
     * @return Collection of records.
     */
    @CallerSensitive
    static <R extends UpdatableRecord<R>, P> Collection<R> records(Configuration configuration, Table<R> table, Collection<P> objects, boolean forUpdate) {
        Collection<R> result = []
        Field<?>[] pk = pk(table)

        for (P object : objects) {
            R record = using(configuration).newRecord(table, object)

            if (forUpdate && pk != null)
                for (Field<?> field : pk)
                    record.changed(field, false)

            resetChangedOnNotNull(record)
            result.add(record)
        }

        return result
    }

    // endregion Public static methods

    // region Private static methods

    /**
     * [#2700] [#3582] If a POJO attribute is NULL, but the column is NOT NULL
     * then we should let the database apply DEFAULT values
     */
    private static void resetChangedOnNotNull(Record record) {
        int size = record.size()

        for (int i = 0; i < size; i++)
            if (record.get(i) == null)
                if (!record.field(i).getDataType().nullable())
                    record.changed(i, false)
    }

    /**
     * Get's the fields that conforms current primary key.
     *
     * @param table {@link Table} instance
     *
     * @return Array of {@link Field}.
     */
    @CallerSensitive
    private static <R extends UpdatableRecord<R>> Field<?>[] pk(Table<R> table) {
        def key = table.getPrimaryKey()

        return key == null ? null : key.getFieldsArray()
    }

    // endregion Private static methods
}
