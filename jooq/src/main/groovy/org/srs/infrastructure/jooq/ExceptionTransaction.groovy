package org.srs.infrastructure.jooq

import org.jooq.ExecuteContext
import org.jooq.SQLDialect
import org.jooq.impl.DefaultExecuteListener
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator
import org.springframework.jdbc.support.SQLExceptionTranslator
import org.springframework.jdbc.support.SQLStateSQLExceptionTranslator

/**
 * Created by diego on 27/09/14.
 * <p/>
 * Exception that occurs when there is a transaction exception.
 */
class ExceptionTransaction extends DefaultExecuteListener {

    /**
     * Generated UID
     */
    private static final long serialVersionUID = -2450323227461061152L

    @Override
    public void exception(ExecuteContext ctx) {
        SQLDialect dialect = ctx.configuration().dialect()

        SQLExceptionTranslator translator = dialect != null ? new SQLErrorCodeSQLExceptionTranslator(dialect.name()) : new SQLStateSQLExceptionTranslator()

        ctx.exception(translator.translate("jOOQ", ctx.sql(), ctx.sqlException()))
    }
}
