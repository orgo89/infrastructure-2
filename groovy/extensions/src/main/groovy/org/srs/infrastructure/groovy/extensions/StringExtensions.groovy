package org.srs.infrastructure.groovy.extensions

/**
 * Contains string extension methods and helpers.
 */
class StringExtensions {

    /**
     * Makes this string empty but not null.
     *
     * @param self The string itself instance.
     */
    static void emptying(String self) {
        self = ''
    }
}
