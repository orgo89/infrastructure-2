Infrastructure 2.0

Contains helper libraries which might be used in any project.

**Note:**
Probably you will think "Why did you make this if already exists?" (maybe
exists on Spring Framework libraries, Apache commons, Google libraries,
Eclipse Foundation or some other open source library). But well... I
started with this library several years ago and I didn't know some of
these libraries or I hadn't used them yet, so I decided in that moment
to create some little libraries to help me in some repetitive tasks
when I develop some application. Another reason was the limited resources
that used to exists for platforms like Android (**in that moment**).

So whatever... enjoy this.

Minimum jdk required 7 or 8 (rather 8, but the com.github.b3er.local.properties
gradle plugin uses java 8, in case java 7 compilation needed take that plugin off).

Projects
========
* **concurrent**

   Contains helper classes to create asynchronous tasks, manage futures
   and complete actions (Some like CompletableFuture but when I made it
   CompletableFuture didn't exists yet XD and used to be annoying try
   to manage asynchronous tasks with Android on versions 5 or less).

* **database**

   Contains helper classes for common data base tasks, configurations or
   actions.

* **dtos**

   Contains basic DTOs for web services. Were made thinking on base and
   common properties that should have any DTO as a response.

* **enums**

   Contains basic and common enumerators. Commonly used them on any
   project.

* **javafx**

   Base classes for JavaFx working. For example to work with model and
   view structure in a easier way.
   
* **jersey**

    Custom features for jersey servlet container like custom properties injection point.

* **repository**

   Classes to implement repositories (for example, in a data base proyect,
   something like basic interfaces from Spring Data).

* **services**

   Classes that contains all my web services (and may contain them yours
   too LOL).

Well, that's all.