package org.srs.infrastructure.exception;

/**
 * @author Diego Lopez
 * <p>
 * An exception that can be thrown in the normal execution
 * of the application and not need to be declared (unchecked exception). Normally used
 * to wrap general unexpected exceptions. This exception expect the application to recover
 * reasonabily although might not the entire flow be recover.
 * This excpetion is the base for all customized unchecked exception.
 */
public class UncheckedApplicationException extends RuntimeException {

    public UncheckedApplicationException() {
    }

    public UncheckedApplicationException(final String message) {
        super(message);
    }

    public UncheckedApplicationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UncheckedApplicationException(final Throwable cause) {
        super(cause);
    }

    public UncheckedApplicationException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
