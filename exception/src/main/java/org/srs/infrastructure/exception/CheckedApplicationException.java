package org.srs.infrastructure.exception;

/**
 * @author Diego Lopez
 * <p>
 * An exception that can be thrown in the normal execution
 * of the application and need to be declared (checked exception). Normally used
 * to wrap general expected exceptions. This exception expect the application to recover
 * completly without any deadlock or interference.
 * This excpetion is the base for all checked exception.
 */
public class CheckedApplicationException extends Exception {

    public CheckedApplicationException() {
    }

    public CheckedApplicationException(final String message) {
        super(message);
    }

    public CheckedApplicationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CheckedApplicationException(final Throwable cause) {
        super(cause);
    }

    public CheckedApplicationException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
