package org.srs.infrastructure.jpa;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Diego Lopez.
 * 
 * Specifies that a field is part of the primary key of an entity.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface KeyPart {
    
    /**
     * Specifies the position of the value of the field annotated, in the generated key.
     * @return Current specified position.
     */
    int position() default 1;
}
