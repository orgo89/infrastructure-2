package org.srs.infrastructure.jpa;

/**
 * @param <T> Specifies the primary key value type, commonly {@link Integer} or {@link Long}.
 *
 *            <p>
 *            Specifies the basic behavior for any keyed entity.
 *            </p>
 * @author Diego López
 */
public interface IBaseEntity<T> {

    /**
     * Gets the current identifier for this entity.
     *
     * @return The current identifier value.
     */
    T getPk();

    /**
     * Sets a new value for the entity identifier.
     *
     * @param pk New identifier value.
     */
    void setPk(T pk);

    /**
     * Fluent setter for this entity primary key.
     *
     * @param pk Primary key.
     * @return Current entity instance.
     */
    IBaseEntity<T> withPk(T pk);
}
