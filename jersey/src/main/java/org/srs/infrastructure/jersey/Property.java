package org.srs.infrastructure.jersey;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * @author Diego López
 *
 * <p>
 * Annotated element value is specified from a properties file.
 * </p>
 */
@Qualifier
@Retention(RUNTIME)
@Target({FIELD, PARAMETER})
public @interface Property {

    /**
     * Property name to look for in properties file.
     */
    String value() default "";
}
