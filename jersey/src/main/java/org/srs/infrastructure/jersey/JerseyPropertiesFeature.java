package org.srs.infrastructure.jersey;

import javax.inject.Singleton;

import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.TypeLiteral;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

/**
 * @author Diego López
 *
 * Allows to register the feature of loading and injecting properties
 * for a jersey jax-rs implementation.
 */
public class JerseyPropertiesFeature extends AbstractBinder {

    @Override
    protected void configure() {
        super.bind(PropertyResolver.class)
            .to(new TypeLiteral<InjectionResolver<Property>>() {})
            .in(Singleton.class);
    }
}