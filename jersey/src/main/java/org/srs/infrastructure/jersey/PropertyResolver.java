package org.srs.infrastructure.jersey;

import org.glassfish.hk2.api.Injectee;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.ServiceHandle;

import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author Diego López
 * <p>
 * Injection resolver to provide the specified property Value specified
 * by {@link Property}.
 */
public class PropertyResolver implements InjectionResolver<Property> {

    // region Fields

    private static final Logger LOG = Logger.getLogger(PropertyResolver.class.getName());

    /**
     * Defines the property name to specify the properties file path to be loaded by
     * this properties resolver.
     */
    public static final String PROPERTIES_FILE_PATH = "PROPERTIES_FILE_PATH";

    /**
     * Properties collection.
     */
    private static Properties properties;

    @Context
    private Configuration configuration;

    // endregion Fields

    // region Constructors

    public PropertyResolver() {
        LOG.info("Constructor called");
    }

    // endregion Constructors

    // region Public methods

    // region InjectionResolver members

    /**
     * This method will return the object that should be injected into the given
     * injection point. It is the responsiblity of the implementation to ensure that
     * the object returned can be safely injected into the injection point.
     * <p>
     * This method should not do the injection themselves
     *
     * @param injectee The injection point this value is being injected into
     * @param root     The service handle of the root class being created, which
     *                 should be used in order to ensure proper destruction of
     *                 associated PerLookup scoped objects. This can be null in
     *                 the case that this is being used for an object not managed by
     *                 HK2. This will only happen if this object is being created
     *                 with the create method of ServiceLocator.
     * @return A possibly null value to be injected into the given injection point
     */
    @Override
    public Object resolve(Injectee injectee, ServiceHandle<?> root) {
        LOG.entering(this.getClass().getName(), "resolve", new Object[]{injectee, root});

        String result = null;

        if (injectee.getRequiredType().getTypeName().equals(String.class.getTypeName())) {
            try {
                if (injectee.getParent() instanceof Field)
                    result = this.getPropertyValue((Field) injectee.getParent());
                else if ((injectee.getParent() instanceof Method) || (injectee.getParent() instanceof Constructor))
                    result = this.getPropertyValue((Executable) injectee.getParent());
            } catch (IOException e) {
                throw new PropertyResolverException(e);
            }
        }

        LOG.exiting(this.getClass().getName(), "resolve", result);

        return result;
    }

    /**
     * This method should return true if the annotation that indicates that this is
     * an injection point can appear in the parameter list of a constructor.
     *
     * @return true if the injection annotation can appear in the parameter list of
     * a constructor
     */
    @Override
    public boolean isConstructorParameterIndicator() {
        return true;
    }

    /**
     * This method should return true if the annotation that indicates that this is
     * an injection point can appear in the parameter list of a method.
     *
     * @return true if the injection annotation can appear in the parameter list of
     * a method
     */
    @Override
    public boolean isMethodParameterIndicator() {
        return true;
    }

    // endregion InjectionResolver members

    // endregion Public methods

    // region Private methods

    /**
     * Gets the current properties collection. It initializes the properties
     * collection lazily.
     *
     * @return A {@link Properties} with all found properties.
     * @throws IOException If properties file does not exist or some error occurs
     *                     reading it.
     */
    private Properties getProperties() throws IOException {
        LOG.entering(this.getClass().getName(), "getProperties");

        if (Objects.isNull(properties)) {
            final String propsFilePath = Optional.ofNullable(configuration.getProperty(PROPERTIES_FILE_PATH))
                    .orElseThrow(() -> new IllegalArgumentException("No PROPERTIES_FILE_PATH property provided"))
                    .toString();

            try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsFilePath)) {
                properties = new Properties();
                properties.load(is);
            }
        }

        LOG.exiting(this.getClass().getName(), "getProperties", properties);

        return properties;
    }

    private String getPropertyValue(Field field) throws IOException {
        return this.getProperties().getProperty(field.getAnnotation(Property.class).value());
    }

    private String getPropertyValue(Executable member) throws IOException {
        final Parameter parameter = Arrays.stream(member.getParameters())
                .filter(p -> p.isAnnotationPresent(Property.class))
                .findFirst()
                .orElse(null);

        String result = null;

        if (parameter != null) {
            result = this.getProperties().getProperty(parameter.getAnnotation(Property.class).value());
        }

        return result;
    }

    // endregion Private methods

}
