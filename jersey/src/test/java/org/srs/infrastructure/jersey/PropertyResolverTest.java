package org.srs.infrastructure.jersey;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.test.JerseyTestNg;
import org.glassfish.jersey.test.TestProperties;
import org.testng.annotations.Test;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;

public class PropertyResolverTest extends JerseyTestNg.ContainerPerClassTest {

    @Override
    protected Application configure() {
        super.forceSet(TestProperties.CONTAINER_PORT, "8213");

        final ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.property(ServerProperties.TRACING, "ALL")
                .property(PropertyResolver.PROPERTIES_FILE_PATH, "test-props.properties")
                .register(new JerseyPropertiesFeature())
                .register(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service.class).to(Service.class);
                    }
                })
                .register(TestResource.class);

        return resourceConfig;
    }

    @Test
    public void testPropertiesLoaded() {
        final Response response = super.target("test").request().get();

        final String expected = response.readEntity(String.class);

        assertEquals(expected, "dummy.value.1 dummy.value.2");
    }

    // Resource must be public as jax-rs rule

    @Path("test")
    public static class TestResource {

        @Property("dummy.property.1")
        private String property1;

        @Inject
        private Service service;

        @GET
        public String get() {
            return this.property1 + " " + this.service.method();
        }
    }

    public static class Service {

        private final String property2;

        public Service(@Property("dummy.property.2") String property2) {
            this.property2 = property2;
        }

        public String method() {
            return this.property2;
        }
    }
}
