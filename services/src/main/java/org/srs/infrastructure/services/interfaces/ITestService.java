package org.srs.infrastructure.services.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by Diego on 13/12/2016.
 * <p>
 * Defines basic operations contract for RESTful web services.
 */
@Path("test")
@Produces("application/json")
public interface ITestService {
    // region Methods

    /**
     * Get's a simple response for this web service.
     *
     * @return {@link Response} object that indicates current service info.
     */
    @GET
    Response test();

    // endregion Methods
}