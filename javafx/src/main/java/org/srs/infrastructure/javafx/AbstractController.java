package org.srs.infrastructure.javafx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

/**
 * <p>
 * Provides base behavior for any JavaFx Controller.
 * The behavior is:
 * Each JavaFx controller can be instantiated by itself.
 * A JavaFx controller can initialized by itself.
 * A JavaFx controller can controlled by itself.
 * This class provides an easy way to create JavaFx windows, actually takes
 * advantages of naming conventions, thus you should have some like:
 * com.package.StageController
 * com.package.Stage.fxml
 * This type of naming convention allow to work automatically.
 */
public abstract class AbstractController {

    // region Protected fields

    /**
     * Current window.
     */
    protected Stage stage;

    // endregion Protected fields

    // region Constructors

    /**
     * <p>
     * Initializes the new controller constructing the {@link Stage}
     * and their FXML.
     * </p>
     *
     * @throws IllegalArgumentException if some parameter is null.
     */
    public AbstractController() throws IllegalArgumentException {
        this(new Stage());
    }

    /**
     * <p>
     * Initializes the new controller constructing the {@link Stage}
     * and their FXML.
     * </p>
     *
     * @param stage An instance of {@link Stage} to be used as window.
     * @throws IllegalArgumentException if some parameter is null.
     */
    public AbstractController(Stage stage) throws IllegalArgumentException {
        if (stage == null)
            throw new IllegalArgumentException("Can not provide a null stage");

        this.stage = stage;
    }

    // endregion Constructors

    // region Public methods

    /**
     * Shows the current {@link Stage}.
     *
     * @throws IllegalStateException - if this method is called on a thread other than the JavaFX Application Thread.
     */
    public void show() throws IllegalStateException {
        this.stage.show();
    }

    /**
     * Hides the current {@link Stage}.
     *
     * @throws IllegalStateException - if this method is called on a thread other than the JavaFX Application Thread.
     */
    public void hide() {
        this.stage.hide();
    }

    /**
     * Closes the current {@link Stage}.
     *
     * @throws IllegalStateException - if this method is called on a thread other than the JavaFX Application Thread.
     */
    public void close() {
        this.stage.close();
    }

    /**
     * Gets the current {@link Stage} for this controller.
     *
     * @return A {@link Stage} object.
     */
    public Stage getStage() {
        return this.stage;
    }

    // endregion Public methods

    // region Protected methods

    /**
     * Initializes the UI.
     *
     * @param controller Specifies the son class. Helper to load the fxml and make controller.
     * @throws IOException if can not find the controller's fxml.
     */
    protected void initUI(Object controller) throws IOException {
        Scene scene = this.initScene(controller);

        this.stage.setScene(scene);
        this.stage.sizeToScene();
        this.stage.centerOnScreen();
    }

    /**
     * @param controller Specifies the son class. Helper to load the fxml and make controller.
     * @return New {@link Scene} instance.
     * @throws IOException if can not find the controller's fxml.
     */
    protected Scene initScene(Object controller) throws IOException {
        String className = controller.getClass().getName();

        className = className.replace(".", "/").replace("Controller", ".fxml");

        URL url = controller.getClass().getResource("/".concat(className));

        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setController(controller);

        Pane pane = (Pane) fxmlLoader.load();

        return new Scene(pane, pane.getPrefWidth(), pane.getPrefHeight());
    }

    // endregion Protected methods

}
