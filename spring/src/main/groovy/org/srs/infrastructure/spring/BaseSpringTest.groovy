package org.srs.infrastructure.spring

import groovy.util.logging.Log4j2
import org.apache.logging.log4j.message.SimpleMessage
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.support.GenericXmlApplicationContext

/**
 * Created by Diego on 28/12/2016.
 *
 * Contains util methods to initialize the spring application context
 * for testing.
 */
@Log4j2
class BaseSpringTest
{
    // region Static properties

    /**
     * Current spring context.
     */
    static ApplicationContext springCtx

    // endregion Static properties

    // region Public static methods

    // region Control test methods

    /**
     * Test class initialization. Spring context set up.
     *
     * @param dbPropertiesFile Relative path to data base configuration properties file.
     * @param annotatedClasses One or more annotated classes, e.g. @Configuration classes.
     */
    static void setUp(String dbPropertiesFile, Class<?>... annotatedClasses)
    {
        def m = log.traceEntry('Method: setUp, Params:\ndbPropertiesFile -> {}\nannotatedClasses -> {}', dbPropertiesFile, annotatedClasses)

        def url = Thread.currentThread().getContextClassLoader().getResource(dbPropertiesFile)

        springCtx = new AnnotationConfigApplicationContext(annotatedClasses)

        log.traceExit(m)
    }

    /**
     * Test class initialization. Spring context set up.
     *
     * @param dbPropertiesFile Relative path to data base configuration properties file.
     * @param xmlConfigFiles One or more xml config files.
     */
    static void setUp(String dbPropertiesFile, String... xmlConfigFiles)
    {
        def m = log.traceEntry('Method: setUp, Params:\ndbPropertiesFile -> {}\nxmlConfigFiles -> {}', dbPropertiesFile, xmlConfigFiles)

        def url = Thread.currentThread().getContextClassLoader().getResource(dbPropertiesFile)

        springCtx = new GenericXmlApplicationContext(xmlConfigFiles)

        log.traceExit(m)
    }

    /**
     * Test class finalization. Spring context set down.
     */
    static void setDown()
    {
        def m = log.traceEntry(new SimpleMessage('Method: setDown'))

        if (springCtx != null)
        {
            springCtx = null
        }

        log.traceExit(m)
    }

    // endregion Public static methods

    // endregion Control test methods
}
