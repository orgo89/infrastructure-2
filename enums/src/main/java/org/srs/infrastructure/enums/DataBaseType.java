package org.srs.infrastructure.enums;

/**
 * Enumerates the data base types.
 */
public enum DataBaseType {

    // region Initializers

    /**
     * Specifies MySQL data base.
     */
    MYSQL("mysql", "com.mysql.jdbc.Driver"),

    /**
     * Specifies MariaDb data base.
     */
    MARIADB("mariadb", "org.mariadb.jdbc.Driver");

    // endregion Initializers

    // region Variables

    /**
     * String with data base alias to use in connection strings.
     */
    private String dbConnectionName;


    /**
     * Driver class name according to the data base.
     */
    private String driverClassName;

    // endregion Variables

    // region Constructors

    /**
     * Initializes the new enum instance.
     *
     * @param dbConnectionName String with data base alias to use in connection strings.
     * @param driverClassName  Driver class name according to the data base.
     */
    DataBaseType(String dbConnectionName, String driverClassName) {
        this.dbConnectionName = dbConnectionName;
        this.driverClassName = driverClassName;
    }

    // endregion Constructors

    // region Public methods

    // region Getters

    public String getDbConnectionName() {
        return dbConnectionName;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    // endregion Getters

    // endregion Public methods

}
