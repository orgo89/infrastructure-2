package org.srs.infrastructure.enums;

/**
 * Represents the status of a process.
 * A process refers the whole joining of actions to get simple or
 * multiple results.
 * Ej. If there is a SALES web service client, all the web service is the
 * process (data base management, business management, any other layer).
 */
public enum ProcessStatus {

    // region Initializers

    /**
     * Indicates a successful process.
     */
    SUCCESSFUL(0),

    /**
     * Indicates a process internal error.
     */
    INTERNALERROR(1),

    /**
     * Indicates that process was failed for external reasons like bad arguments.
     */
    EXTERNALERROR(2);

    // endregion Initializers

    // region Variables

    /**
     * Describes the current process status.
     */
    private int status;

    // endregion Variables

    // region Constructors

    /**
     * Initializes the enum instance.
     *
     * @param status Current process status.
     */
    ProcessStatus(int status) {
        this.status = status;
    }

    // endregion Constructors

    // region Public methods

    // region Getters

    public int getStatus() {
        return status;
    }

    // endregion Getters

    // endregion Public methods

}
