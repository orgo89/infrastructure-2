package org.srs.infrastructure.enums;

/**
 * Represents possible operation status.
 * An operation refers to a specific action, maybe in the whole process.
 * Ej. If there is a SALES web service client, all the web service is the
 * process (data base management, business management, any other layer)
 * but the "SALE" action is the operation. So the client maybe finishes
 * successfully but the SALE won't be concreted.
 */
public enum OperationStatus {

    // region Initializers

    SUCCESSFUL(true), UNSUCCESSFUL(false);

    // endregion Initializers

    // region Variables

    /**
     * Describes an operation status.
     */
    private boolean state;

    // endregion Variables

    //  region Constructors

    /**
     * Initializes the new enum instance.
     *
     * @param state Operation status.
     */
    OperationStatus(boolean state) {
        this.state = state;
    }

    //  endregion Constructors

    // region Public methods

    // region Getters

    public boolean isState() {
        return state;
    }

    // endregion Getters

    // endregion Public methods

}
