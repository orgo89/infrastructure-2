package org.srs.infrastructure.repository;


import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.Persistence;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class AbstractJpaCrudRepositoryTest {

    private SimpleEntityRepository rep;
    private Integer pk;

    @BeforeClass
    public void init() {
        this.rep = new SimpleEntityRepository(
                Persistence.createEntityManagerFactory("repository").createEntityManager()
                , SimpleEntity.class
        );
    }

    @AfterClass
    public void finit() {
        this.rep.entityManager.close();
    }

    private SimpleEntity provider() {
        SimpleEntity se = new SimpleEntity();

        se.setData1("simple data 1");
        se.setData2("simple data 2");

        SimpleChildEntity sce = new SimpleChildEntity();

        sce.setData1("simple child data 1");
        sce.setData2("simple child data 2");

        se.addChild(sce);

        return se;
    }

    @Test
    public void insert() {
        SimpleEntity entity = this.provider();

        this.rep.save(entity);
        this.pk = entity.getPk();

        assertNotNull(this.pk);
    }

    @Test(priority = 1)
    public void getAll() {
        final List<SimpleEntity> all = this.rep.getAll();
        final Stream<SimpleEntity> allStream = this.rep.getAllAsStream();

        assertTrue(!all.isEmpty() && allStream.count() > 0);
    }

    @Test(priority = 2)
    public void update() {
        final SimpleEntity se = this.rep
                .getById(this.pk)
                .orElseThrow(() -> new AssertionError("No entity found"));

        se.setData1("Modified data 1");

        SimpleEntity use = this.rep.update(se);

        Assert.assertEquals(use, se);
    }

    @Test(priority = 3)
    public void delete() {
        final SimpleEntity se = this.rep
                .getById(this.pk)
                .orElseThrow(() -> new AssertionError("No entity found"));

        this.rep.delete(se);

        final Optional<SimpleEntity> se2 = this.rep
                .getById(this.pk);

        Assert.assertFalse(se2.isPresent());
    }
}
