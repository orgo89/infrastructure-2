package org.srs.infrastructure.repository;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
class SimpleChildEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer pk;

    private String data1;

    private String data2;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    private SimpleEntity parent;

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public SimpleEntity getParent() {
        return parent;
    }

    public void setParent(SimpleEntity parent) {
        this.parent = parent;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleChildEntity)) return false;
        SimpleChildEntity that = (SimpleChildEntity) o;
        return Objects.equals(getPk(), that.getPk()) &&
                Objects.equals(getData1(), that.getData1()) &&
                Objects.equals(getData2(), that.getData2()) &&
                Objects.equals(getParent(), that.getParent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPk(), getData1(), getData2(), getParent());
    }

    @Override
    public String toString() {
        return "SimpleChildEntity{" +
                "pk=" + pk +
                ", data1='" + data1 + '\'' +
                ", data2='" + data2 + '\'' +
                ", parent=" + parent +
                '}';
    }
}
