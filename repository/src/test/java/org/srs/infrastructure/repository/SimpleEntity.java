package org.srs.infrastructure.repository;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
class SimpleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer pk;

    private String data1;

    private String data2;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "parent", orphanRemoval = true)
    private List<SimpleChildEntity> childs;

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public List<SimpleChildEntity> getChilds() {
        return childs;
    }

    public void setChilds(List<SimpleChildEntity> childs) {
        this.childs = childs;
    }

    public SimpleChildEntity addChild(SimpleChildEntity child) {
        if (this.childs == null)
            this.childs = new ArrayList<>();

        this.getChilds().add(child);
        child.setParent(this);

        return child;
    }

    public SimpleChildEntity removeChild(SimpleChildEntity child) {
        getChilds().remove(child);
        child.setParent(null);

        return child;
    }

    @Override
    public String toString() {
        return "SimpleEntity{" +
                "pk=" + pk +
                ", data1='" + data1 + '\'' +
                ", data2='" + data2 + '\'' +
                ", childs=" + childs +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleEntity)) return false;
        SimpleEntity that = (SimpleEntity) o;
        return Objects.equals(getPk(), that.getPk()) &&
                Objects.equals(getData1(), that.getData1()) &&
                Objects.equals(getData2(), that.getData2()) &&
                Objects.equals(getChilds(), that.getChilds());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPk(), getData1(), getData2(), getChilds());
    }
}
