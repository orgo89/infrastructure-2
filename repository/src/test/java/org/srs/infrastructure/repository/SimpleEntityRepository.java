package org.srs.infrastructure.repository;

import javax.persistence.EntityManager;

class SimpleEntityRepository extends AbstractJpaCrudRepository<SimpleEntity, Integer> {

    public SimpleEntityRepository(EntityManager entityManager, Class<SimpleEntity> contextType) throws IllegalArgumentException {
        super(entityManager, contextType);
    }
}
