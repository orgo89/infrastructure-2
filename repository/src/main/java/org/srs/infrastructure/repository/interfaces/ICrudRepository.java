package org.srs.infrastructure.repository.interfaces;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by diego on 21/04/15.
 * <br>
 * Defines the base contract for CRUD repositories.
 */
public interface ICrudRepository<T, K> {

    // region Methods

    /**
     * Save the specified entity within a database transaction.
     *
     * @param entity Entity to be created.
     */
    void saveInTx(T entity);

    /**
     * Save the specified entity to database.
     *
     * @param entity Entity to be created.
     */
    void save(T entity);

    /**
     * Save the specified entities to database within the current transaction.
     *
     * @param entities Entities to create.
     */
    void saveAllInTx(List<T> entities);

    /**
     * Save the specified entities to database.
     *
     * @param entities Entities to create.
     */
    void saveAll(List<T> entities);

    /**
     * Updates an exist entity within a database transaction.
     *
     * @param entity Entity to be updated.
     */
    void updateInTx(T entity);

    /**
     * Updates an exist entity to database.
     *
     * @param entity Entity to be updated.
     * @return A new instance of T attached to the current {@link EntityManager}.
     */
    T update(T entity);

    /**
     * Deletes the specified entity within a database transaction.
     *
     * @param entity Entity to be deleted.
     */
    void deleteInTx(T entity);

    /**
     * Deletes the specified entity from database
     * .
     *
     * @param entity Object to be deleted.
     */
    void delete(T entity);

    /**
     * Deletes all entities.
     */
    void deleteAll(List<T> entities);

    /**
     * Deletes all entities without commit.
     */
    void deleteAllInTx(List<T> entities);

    /**
     * Obtains all the entities.
     *
     * @return {@link List} of entities.
     */
    List<T> getAll();

    /**
     * Gets all the entities into a stream.
     *
     * @return {@link Stream} of entities.
     */
    Stream<T> getAllAsStream();

    /**
     * Gets the entity with given id.
     *
     * @param id Id to search.
     * @return Entity found.
     */
    Optional<T> getById(K id);

    // endregion Methods

}
