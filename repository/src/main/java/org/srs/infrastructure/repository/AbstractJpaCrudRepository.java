package org.srs.infrastructure.repository;

import org.srs.infrastructure.repository.interfaces.ICrudRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Defines the base behavior for CRUD repositories.
 * Extends this class is useful to create repositories with only some
 * CRUD methods. If it needs to create a full repository should be
 * better implements {@link ICrudRepository}.
 * <p>
 * This abstract repository lacks of constructors because enterprise java beans
 * can't use constructors for injection purposes.
 *
 * @param <T> Entity type.
 * @param <K> Primary key type.
 */
public abstract class AbstractJpaCrudRepository<T, K> implements ICrudRepository<T, K> {

    // region Private fields

    /**
     * Current contextual entity type.
     */
    private Class<T> contextType;

    // endregion Private fields

    // region Protected fields

    /**
     * Current entity manager for JPA actions.
     */
    protected EntityManager entityManager;

    // endregion Protected fields

    // region Constructors

    /**
     * Initializes the current repository without {@link EntityManager}.
     * This is convenient way for java cdi standard.
     */
    public AbstractJpaCrudRepository() {
    }

    /**
     * Initializes the current repository.
     *
     * @param entityManager Especifies the current {@link EntityManager}.
     * @throws IllegalArgumentException If supply null parameter.
     */
    public AbstractJpaCrudRepository(EntityManager entityManager) throws IllegalArgumentException {
        if (entityManager == null)
            throw new IllegalArgumentException("You can not supply a null EntityManager");

        this.entityManager = entityManager;
    }

    /**
     * Initializes the current repository.
     *
     * @param contextType Current contextual entity type.
     */
    public AbstractJpaCrudRepository(Class<T> contextType) {
        if (contextType == null)
            throw new IllegalArgumentException("You can not supply a null context type");

        this.contextType = contextType;
    }

    /**
     * Initializes the current repository.
     *
     * @param entityManager Especifies the current {@link EntityManager}.
     * @param contextType   Current contextual entity type.
     * @throws IllegalArgumentException If supply some null parameter.
     */
    public AbstractJpaCrudRepository(EntityManager entityManager, Class<T> contextType) throws IllegalArgumentException {
        this(entityManager);

        if (contextType == null)
            throw new IllegalArgumentException("You can not supply a null contextual type");

        this.contextType = contextType;
    }


    // endregion Constructors

    // region Public methods

    // region ICrudRepository members

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#saveInTx(Object)
     * @see EntityManager#persist(Object)
     */
    @Override
    public void saveInTx(T entity) {
        this.entityManager.persist(entity);
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#save(Object)
     * @see #beginTransaction()
     * @see EntityManager#persist(Object)
     * @see #commitTransaction()
     */
    @Override
    public void save(T entity) {
        this.beginTransaction();
        this.entityManager.persist(entity);
        this.commitTransaction();
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#saveAllInTx(List)
     */
    @Override
    public void saveAllInTx(List<T> entities) {
        for (T entity : entities) {
            this.entityManager.persist(entity);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#saveAll(List)
     */
    @Override
    public void saveAll(List<T> entities) {
        this.beginTransaction();
        for (T entity : entities) {
            this.entityManager.persist(entity);
        }
        this.commitTransaction();
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#updateInTx(Object)
     */
    @Override
    public void updateInTx(T entity) {
        this.entityManager.persist(entity);
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#update(Object)
     * @see #beginTransaction()
     * @see EntityManager#merge(Object)
     * @see #commitTransaction()
     */
    @Override
    public T update(T entity) {
        this.beginTransaction();
        T newAttachedEntity = this.entityManager.merge(entity);
        this.commitTransaction();

        return newAttachedEntity;
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#deleteInTx(Object)
     * @see EntityManager#remove(Object)
     */
    @Override
    public void deleteInTx(T entity) {
        this.entityManager.remove(entity);
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#delete(Object)
     * @see #beginTransaction()
     * @see EntityManager#remove(Object)
     * @see #commitTransaction()
     */
    @Override
    public void delete(T entity) {
        this.beginTransaction();
        this.entityManager.remove(entity);
        this.commitTransaction();
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#deleteAll(List)
     */
    @Override
    public void deleteAll(List<T> entities) {
        this.beginTransaction();
        for (T entity : entities) {
            this.entityManager.remove(entity);
        }
        this.commitTransaction();
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#deleteAllInTx(List)
     */
    public void deleteAllInTx(List<T> entities) {
        for (T entity : entities) {
            this.entityManager.remove(entity);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#getAll()
     */
    @Override
    public List<T> getAll() {
        CriteriaQuery<T> query = this.entityManager.getCriteriaBuilder().createQuery(this.contextType);
        Root<T> from = query.from(this.contextType);
        CriteriaQuery<T> select = query.select(from);
        TypedQuery<T> tquery = this.entityManager.createQuery(select);

        return tquery.getResultList();
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#getAllAsStream()
     */
    @Override
    public Stream<T> getAllAsStream() {
        CriteriaQuery<T> query = this.entityManager.getCriteriaBuilder().createQuery(this.contextType);
        Root<T> from = query.from(this.contextType);
        CriteriaQuery<T> select = query.select(from);
        TypedQuery<T> tquery = this.entityManager.createQuery(select);

        return tquery.getResultStream();
    }

    /**
     * (non-Javadoc)
     *
     * @see ICrudRepository#getById(Object)
     * @see EntityManager#find(Class, Object)
     */
    @Override
    public Optional<T> getById(K id) {
        return Optional.ofNullable(this.entityManager.find(this.contextType, id));
    }

    // endregion ICrudRepository members

    /**
     * Convenience method to initiate a new database transaction.
     *
     * @see EntityManager#getTransaction()
     * @see EntityTransaction#begin()
     */
    public void beginTransaction() {
        this.entityManager.getTransaction().begin();
    }

    /**
     * Convenience method that attempts to commit the current database transaction.
     *
     * @see EntityManager#getTransaction()
     * @see EntityTransaction#commit()
     */
    public void commitTransaction() {
        this.entityManager.getTransaction().commit();
    }

    /**
     * Convenience method to rollback the current database transaction.
     *
     * @see EntityManager#getTransaction()
     * @see EntityTransaction#rollback()
     */
    public void rollbackTransaction() {
        if (this.entityManager.getTransaction().isActive())
            this.entityManager.getTransaction().rollback();
    }

    /**
     * @see EntityManager#flush()
     * Also check if there is an active transaction to execute
     * the {@link EntityManager#flush()} method to avoid a
     * {@link javax.persistence.TransactionRequiredException}.
     */
    public void flush() {
        if (this.entityManager.getTransaction().isActive())
            this.entityManager.flush();
    }

    // endregion Public methods

}
